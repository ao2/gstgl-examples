/*
 * common-opengl-render-to-texture - Render to a texture in OpenGL
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>

#ifndef DISABLE_GLFW
#include <GLFW/glfw3.h>
#endif

#include <cglm/cglm.h>

int width = 500;
int height = 500;

GLuint scene_texture;
static GLuint scene_fbo;

typedef struct Vertex
{
  vec2 pos;
  vec3 col;
} Vertex;

static const Vertex vertices[3] = {
  /* Equilateral triangle on the unit circle. */
  {{-0.866f, -0.5f}, {1.f, 0.f, 0.f}},
  {{0.866f, -0.5f}, {0.f, 1.f, 0.f}},
  {{0.0f, 1.0f}, {0.f, 0.f, 1.f}},
};

static GLuint model_vao;
static GLuint model_program;
static GLint mvp_location;

static GLuint quad_vao;
static GLuint quad_program;
static GLuint texID;

static const char *model_vertex_shader_text =
    "#version 330\n"
    "uniform mat4 MVP;\n"
    "in vec2 vPos;\n"
    "in vec3 vCol;\n"
    "out vec3 color;\n"
    "void main()\n"
    "{\n"
    "    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
    "    color = vCol;\n"
    "}\n";

static const char *model_fragment_shader_text =
    "#version 330\n"
    "in vec3 color;\n"
    "layout(location = 0) out vec4 fragment;\n"
    "void main()\n"
    "{\n"
    "    fragment = vec4(color, 1.0);\n"
    "}\n";

static const char *passthrough_vertex_shader_text =
    "#version 330 core\n"
    "layout(location = 0) in vec3 vertexPosition_modelspace;\n"
    "void main()\n"
    "{\n"
    "	gl_Position =  vec4(vertexPosition_modelspace, 1);\n"
    "}\n";

static const char *passthrough_fragment_shader_text =
    "#version 330 core\n"
    "uniform sampler2D renderedTexture;\n"
    "out vec3 color;\n"
    "void main()\n"
    "{\n"
    "	vec2 size = textureSize(renderedTexture, 0);\n"
    "	color = texture(renderedTexture, gl_FragCoord.xy / size).xyz;\n"
    "}\n";

#define SCREENSHOT_MAX_FILENAME 256
static char screenshot_filename[SCREENSHOT_MAX_FILENAME];
static GLubyte *pixels = NULL;

static unsigned int nframes = 0;
static double time0;

/* Model. */
static float angle;
static float delta_angle;

/*
 * Take screenshot with glReadPixels and save to a file in PPM format.
 *
 * The data is read a way that flips the image vertically when writing to the
 * file to accommodate for the OpenGL coordinate system having the origin at
 * the bottom-left corner.
 *
 * - filename: file path to save to, without extension
 * - width: screen width in pixels
 * - height: screen height in pixels
 * - pixels: intermediate buffer to avoid repeated mallocs across multiple calls.
 *   Contents of this buffer do not matter. May be NULL, in which case it is initialized.
 *   You must `free` it when you won't be calling this function anymore.
 */
static void
screenshot_ppm (const char *filename, int image_width,
    int image_height, GLubyte ** image_pixels)
{
  int i, j;
  size_t cur;
  const size_t format_nchannels = 3;
  FILE *f = fopen (filename, "w");
  fprintf (f, "P3\n%d %d\n%d\n", image_width, image_height, 255);
  *image_pixels =
      realloc (*image_pixels,
      format_nchannels * sizeof (GLubyte) * image_width * image_height);
  glReadPixels (0, 0, image_width, image_height, GL_RGB, GL_UNSIGNED_BYTE,
      *image_pixels);
  for (i = 0; i < image_height; i++) {
    for (j = 0; j < image_width; j++) {
      cur = format_nchannels * ((image_height - i - 1) * image_width + j);
      fprintf (f, "%3d %3d %3d ", (*image_pixels)[cur],
          (*image_pixels)[cur + 1], (*image_pixels)[cur + 2]);
    }
    fprintf (f, "\n");
  }
  fclose (f);
}

static void
model_init (void)
{
  angle = 0;
  delta_angle = 0.05;
}

int
model_update (void)
{
  angle += delta_angle;
  return 0;
}

static GLuint
compile_shader (GLenum shader_type, const char *shader_text)
{
  const GLuint shader = glCreateShader (shader_type);
  glShaderSource (shader, 1, &shader_text, NULL);
  glCompileShader (shader);

  GLint is_compiled = 0;
  glGetShaderiv (shader, GL_COMPILE_STATUS, &is_compiled);
  if (is_compiled == GL_FALSE) {
    GLint max_length = 0;
    glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &max_length);

    GLchar error_log[max_length];
    GLint error_length = 0;
    glGetShaderInfoLog (shader, max_length, &error_length, &error_log[0]);

    if (error_length <= 0) {
	fprintf (stderr, "Unexpected shader error\n");
    } else {
	fprintf (stderr, "Shader compilation error: %s%c", error_log,
		error_log[error_length - 1] == '\n' ? '\0' : '\n');
    }

    glDeleteShader (shader);
    exit (EXIT_FAILURE);
  }

  return shader;
}

static void
scene_init (void)
{
  /* Shaders setup. */
  const GLuint model_vertex_shader =
      compile_shader (GL_VERTEX_SHADER, model_vertex_shader_text);
  const GLuint model_fragment_shader =
      compile_shader (GL_FRAGMENT_SHADER, model_fragment_shader_text);

  model_program = glCreateProgram ();
  glAttachShader (model_program, model_vertex_shader);
  glAttachShader (model_program, model_fragment_shader);
  glLinkProgram (model_program);

  mvp_location = glGetUniformLocation (model_program, "MVP");
  const GLint vpos_location = glGetAttribLocation (model_program, "vPos");
  const GLint vcol_location = glGetAttribLocation (model_program, "vCol");

  /* Scene setup. */
  GLuint vertex_buffer;
  glGenBuffers (1, &vertex_buffer);
  glBindBuffer (GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_STATIC_DRAW);

  glGenVertexArrays (1, &model_vao);
  glBindVertexArray (model_vao);
  glEnableVertexAttribArray (vpos_location);
  glVertexAttribPointer (vpos_location, 2, GL_FLOAT, GL_FALSE,
      sizeof (Vertex), (void *) offsetof (Vertex, pos));
  glEnableVertexAttribArray (vcol_location);
  glVertexAttribPointer (vcol_location, 3, GL_FLOAT, GL_FALSE,
      sizeof (Vertex), (void *) offsetof (Vertex, col));

  /* Offscreen rendering framebuffer. */
  glGenFramebuffers (1, &scene_fbo);
  glBindFramebuffer (GL_FRAMEBUFFER, scene_fbo);

  /*
   * Color Texture.
   *
   * IMPORTANT: create a *complete* texture with only one mipmap level.
   */
  glGenTextures (1, &scene_texture);
  glBindTexture (GL_TEXTURE_2D, scene_texture);
  glTexStorage2D (GL_TEXTURE_2D, 1, GL_RGB8, width, height);
  glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB,
      GL_UNSIGNED_BYTE, NULL);
  glBindTexture (GL_TEXTURE_2D, 0);

  /* 
   * Attach empty texture to framebuffer object: drawing to scene_fbo will use
   * scene_texture as the backing storage.
   */
  glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
      scene_texture, 0);

  glReadBuffer (GL_COLOR_ATTACHMENT0);

  GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers (1, DrawBuffers);

  /* Sanity check. */
  if (glCheckFramebufferStatus (GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    fprintf (stderr, "glCheckFramebufferStatus() failed.");
    exit (0);
  }

  glBindFramebuffer (GL_FRAMEBUFFER, 0);
}

static void
scene_deinit()
{
  glDeleteFramebuffers (1, &scene_fbo);
  glDeleteTextures (1, &scene_texture);
}

static void
viewport_init (void)
{
  /* Set up the fullscreen quad. */
  glGenVertexArrays (1, &quad_vao);
  glBindVertexArray (quad_vao);

  const GLfloat vertices[] = {
    -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f
  };

  GLuint quad_vbo;
  glGenBuffers (1, &quad_vbo);
  glBindBuffer (GL_ARRAY_BUFFER, quad_vbo);
  glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_STATIC_DRAW);

  glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray (0);

  glBindVertexArray (0);
  glDeleteBuffers (1, &quad_vbo);

  /* Quad program shaders */
  const GLuint quad_vertex_shader =
      compile_shader (GL_VERTEX_SHADER, passthrough_vertex_shader_text);
  const GLuint quad_fragment_shader =
      compile_shader (GL_FRAGMENT_SHADER, passthrough_fragment_shader_text);

  quad_program = glCreateProgram ();
  glAttachShader (quad_program, quad_vertex_shader);
  glAttachShader (quad_program, quad_fragment_shader);
  glLinkProgram (quad_program);

  texID = glGetUniformLocation (quad_program, "renderedTexture");
}

void
init (void)
{
  model_init ();
  scene_init ();

  viewport_init ();

#ifndef DISABLE_GLFW
  time0 = glfwGetTime ();
#endif
}

void
deinit (void)
{
#ifndef DISABLE_GLFW
  printf ("FPS = %f\n", (double) nframes / (glfwGetTime () - time0));
#endif
  free (pixels);

  scene_deinit();
}

static void
draw_scene (void)
{
  /*
   * Set the viewport to the texture size, in this case it just happens
   * to be the same of the window.
   */
  glViewport (0, 0, width, height);

  glClearColor (1.0, 1.0, 0.0, 1.0);
  glClear (GL_COLOR_BUFFER_BIT);

  mat4 m, p, mvp;

  glm_mat4_identity (m);
  glm_rotate_z (m, angle, m);

  float ratio = width / height;
  glm_ortho (-ratio, ratio, -1.f, 1.f, 1.f, -1.f, p);

  glm_mat4_mul (p, m, mvp);

  glUseProgram (model_program);
  glUniformMatrix4fv (mvp_location, 1, GL_FALSE, (const GLfloat *) mvp);
  glBindVertexArray (model_vao);
  glDrawArrays (GL_TRIANGLES, 0, 3);
}

void
draw_screen (void)
{
  /* Draw to the offscreen framebuffer. */
  glBindFramebuffer (GL_FRAMEBUFFER, scene_fbo);
  draw_scene ();
  glFlush ();

  /* Also render to the screen */
  glBindFramebuffer (GL_FRAMEBUFFER, 0);

  /* Render on the whole framebuffer, from the lower left corner to the upper right. */
  glViewport (0, 0, width, height);

  /* Clear the screen with gray, this won't be visible as our off-screen fb has a yellow background. */
  glClearColor (0.5, 0.5, 0.5, 1.0);
  glClear (GL_COLOR_BUFFER_BIT);

  /* Use the pass-through shader to render to a screen-mapped quad. */
  glUseProgram (quad_program);

  /* Bind the texture associated with the offscreen fbo to Texture Unit 0 */
  glActiveTexture (GL_TEXTURE0);
  glBindTexture (GL_TEXTURE_2D, scene_texture);

  /* Set the "renderedTexture" sampler to use Texture Unit 0 */
  glUniform1i (texID, 0);

  /* Draw the quad with the mapped texture. */
  glBindVertexArray (quad_vao);
  glDrawArrays (GL_TRIANGLE_STRIP, 0, 4);

  glBindTexture (GL_TEXTURE_2D, 0);

  nframes++;
}

#ifndef DISABLE_GLFW
void
input_cb (GLFWwindow * window, int key, int scancode, int action, int mods)
{
  (void) scancode;
  (void) mods;

  switch (key) {
    case GLFW_KEY_ESCAPE:
    case GLFW_KEY_Q:
      if (action == GLFW_PRESS) {
        glfwSetWindowShouldClose (window, GLFW_TRUE);
      }
      break;

    case GLFW_KEY_S:
      if (key == GLFW_KEY_S && action == GLFW_PRESS) {
        snprintf (screenshot_filename, SCREENSHOT_MAX_FILENAME,
            "frame-%05d.ppm", nframes);

        glBindFramebuffer (GL_FRAMEBUFFER, scene_fbo);
        screenshot_ppm (screenshot_filename, width, height, &pixels);
        glBindFramebuffer (GL_FRAMEBUFFER, 0);
      }
      break;

    default:
      break;
  }
}
#endif

void
error_callback (int error, const char *description)
{
  (void) error;
  fprintf (stderr, "Error: %s\n", description);
}

void GLAPIENTRY
debug_callback (GLenum source, GLenum type, GLuint id,
    GLenum severity, GLsizei length,
    const GLchar * message, const void *userParam)
{
  (void) source;
  (void) id;
  (void) length;
  (void) userParam;
  fprintf (stderr,
      "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
      (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type, severity,
      message);
}
