/*
 * opengl-render-to-texture - Render to a texture in OpenGL and pass it to GStreamer
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdlib.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GLFW/glfw3.h>

#include "common-opengl-render-to-texture.h"

int
main (int argc, char *argv[])
{
  (void) argc;
  GLFWwindow *window;

  glfwSetErrorCallback (error_callback);

  if (!glfwInit ())
    exit (EXIT_FAILURE);

  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint (GLFW_RESIZABLE, 0);

  window = glfwCreateWindow (width, height, argv[0], NULL, NULL);
  if (!window) {
    glfwTerminate ();
    exit (EXIT_FAILURE);
  }

  glfwSetKeyCallback (window, input_cb);

  glfwMakeContextCurrent (window);
  glfwSwapInterval (1);

  glEnable (GL_DEBUG_OUTPUT);
  glDebugMessageCallback (debug_callback, NULL);

  init ();

  while (!glfwWindowShouldClose (window)) {
    glfwGetFramebufferSize (window, &width, &height);
    glViewport (0, 0, width, height);

    model_update ();
    draw_screen ();

    glfwSwapBuffers (window);
    glfwPollEvents ();
  }

  deinit ();

  glfwDestroyWindow (window);

  glfwTerminate ();
  exit (EXIT_SUCCESS);
}
