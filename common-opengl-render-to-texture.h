/*
 * common-opengl-render-to-texture - Render to a texture in OpenGL
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <GL/gl.h>

#ifndef DISABLE_GLFW
#include <GLFW/glfw3.h>
#endif

extern int width;
extern int height;
extern GLuint scene_texture;

int model_update (void);

void init (void);

void deinit (void);

void draw_screen (void);

#ifndef DISABLE_GLFW
void input_cb (GLFWwindow * window, int key, int scancode, int action,
    int mods);
#endif

void error_callback (int error, const char *description);

void GLAPIENTRY debug_callback (GLenum source, GLenum type, GLuint id,
    GLenum severity, GLsizei length,
    const GLchar * message, const void *userParam);
