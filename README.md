# Collection of examples about how to use OpenGL and GStreamer together

Some examples may depend on some features from GStreamer 1.20, gst-buid devenv can be used for that.

They can be built with the following commands:

```
$ meson build/
$ ninja -C build/
```

The executables will be available under the `build/` directory.

## gst-gears

An example based on `glfilterapp` to show the simplest way to render a custom OpenGL scene in a GStreamer pipeline.


## opengl-render-to-texture

A general OpenGL render-to-texture example without GStreamer integration.

This is basically to recap how rendering to a texture works in OpenGL.

Render a scene with a rotating triangle to an off-screen texture and then render this texture in a full-screen quad for display purposes.

The example only shows how to render off-screen, but in a real-world scenario the texture can be used for further processing, like for example being passed to GStreamer for encoding.

References:

<https://www.songho.ca/opengl/gl_fbo.html>
<https://www.khronos.org/opengl/wiki/Framebuffer_Object_Extension_Examples>
<https://www.khronos.org/opengl/wiki/Common_Mistakes>


## gstgl-shared-context-appsrc-opengl-texture

An example about sharing an OpenGL context between an exiting OpenGL program and a GStreamer pipeline using `appsrc` to push OpenGL textures into the pipeline.

In this case the OpenGL context is created by the glfw toolkit and shared with GStreamer.


## gstgl-exclusive-context-rtsp-server-appsrc-opengl-texture

A very simple remote rendering example where the rendered OpenGL scene is encoded and made available via RTSP.

In this case the OpenGL context is created by GStreamer itself.
