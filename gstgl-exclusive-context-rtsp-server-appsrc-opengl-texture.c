/*
 * gst-rtsp-server-opengl-texture - Render to a texture in OpenGL and pass it to GstRtspServer
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>

#include <gst/gst.h>
#include <gst/rtsp-server/rtsp-server.h>

#include <gst/gl/gl.h>
#include <gst/app/gstappsrc.h>
//#include <gst/gl/x11/gstgldisplay_x11.h>
#include <gst/gl/gstgldisplay.h>
#include <gst/gl/gstglmemory.h>

#include "common-opengl-render-to-texture.h"

gboolean
signal_handler (gpointer user_data)
{
  GMainLoop * loop = (GMainLoop *)user_data;

  g_print ("Interrupt received, closing...\n");
  g_main_loop_quit (loop);

  return TRUE;
}

typedef struct app_t
{
  GstGLDisplay *gl_display;

  GstGLContext *gl_context;
  GstContext *display_context;
  GstContext *app_context;

  GstGLVideoAllocationParams *allocation_params;

  int width;
  int height;

  GLuint framebuffer;
  GLuint texture;

  GstPipeline *pipeline;
  GMainLoop *loop;
  GstElement *source;
  guint sourceid;
} app_t;

app_t g_app = { 0 };

/* Prepare a GstBuffer, wrap an OpenGL texture into it. */
static GstBuffer *
wrap_gl_texture (app_t * app)
{
  GstGLMemoryAllocator *allocator;
  gpointer wrapped[1];
  GstGLFormat formats[1];
  GstBuffer *buffer;
  gboolean ret;

  allocator = gst_gl_memory_allocator_get_default (app->gl_context);

  buffer = gst_buffer_new ();
  if (!buffer) {
    g_error ("Failed to create new buffer\n");
    return NULL;
  }

  wrapped[0] = (gpointer) app->texture;
  formats[0] = GST_GL_RGBA8;

  /* Wrap the texture into GLMemory. */
  ret = gst_gl_memory_setup_buffer (allocator, buffer, app->allocation_params,
      formats, wrapped, 1);
  if (!ret) {
    g_error ("Failed to setup gl memory\n");
    return NULL;
  }

  gst_object_unref (allocator);

  return buffer;
}

/* render the scene and push the buffer */
static void
_gst_gl_draw_frame (app_t * app)
{
  GstBuffer *buffer;
  GstFlowReturn ret;

  /* Render the scene. */

  model_update ();
  draw_screen ();

  /* Wrap the rendered texture into a GstBuffer. */
  buffer = wrap_gl_texture (app);

  /* Sync on the buffer (this is probably most useful when using a buffer-pool). */
  GstGLSyncMeta *sync_meta = gst_buffer_get_gl_sync_meta (buffer);
  if (sync_meta) {
    gst_gl_sync_meta_set_sync_point (sync_meta, app->gl_context);
    gst_gl_sync_meta_wait (sync_meta, app->gl_context);
  }

  /* Push the buffer. */
  ret = gst_app_src_push_buffer (GST_APP_SRC (app->source), buffer);
  if (ret != GST_FLOW_OK) {
    g_printerr ("GST_FLOW != OK, return value is %d\n", ret);
  }
}

void _draw_cb (GstGLContext * context, gpointer * userdata) {
  (void)context;
  (void)userdata;

  _gst_gl_draw_frame (&g_app);
}

static gboolean
_gst_idle_loop (gpointer data)
{
(void)data;

   gst_gl_context_thread_add (g_app.gl_context,
                               (GstGLContextThreadFunc) _draw_cb, NULL);

  return TRUE;
}

static void
_gst_gl_init (app_t * app, int width, int height)
{
  GstVideoInfo *vinfo = gst_video_info_new ();

  gboolean ret =
      gst_video_info_set_format (vinfo, GST_VIDEO_FORMAT_RGBA, width, height);
  if (!ret) {
    fprintf (stderr, "set video info ret %d\n", ret);
    exit (EXIT_FAILURE);
  }

  app->allocation_params =
      gst_gl_video_allocation_params_new_wrapped_texture (app->gl_context, NULL,
      vinfo, 0, NULL, GST_GL_TEXTURE_TARGET_2D, GST_GL_RGBA8, app->texture,
      NULL, 0);
}

#if 0
static gboolean
_gst_bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
  app_t *app = (app_t *) data;

  (void) bus;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      fprintf (stderr, "eos\n");
      g_main_loop_quit (app->loop);
      break;

    case GST_MESSAGE_ERROR:
    {
      gchar *debug = NULL;
      GError *err = NULL;

      gst_message_parse_error (msg, &err, &debug);

      fprintf (stderr, "error '%s'\n", err->message);
      g_error_free (err);

      if (debug) {
        fprintf (stderr, "deails '%s'\n", debug);
        g_free (debug);
      }

      if (app->loop)
        g_main_loop_quit (app->loop);
      else
        exit (EXIT_FAILURE);
      break;
    }

    case GST_MESSAGE_NEED_CONTEXT:
    {
      const gchar *context_type;

      gst_message_parse_context_type (msg, &context_type);
      fprintf (stderr, "got need context %s from element %s\n", context_type,
          GST_ELEMENT_NAME (GST_ELEMENT (msg->src)));

      if (g_strcmp0 (context_type, GST_GL_DISPLAY_CONTEXT_TYPE) == 0) {
        gst_element_set_context (GST_ELEMENT (msg->src), g_app.display_context);
      } else if (g_strcmp0 (context_type, "gst.gl.app_context") == 0) {
        gst_element_set_context (GST_ELEMENT (msg->src), g_app.app_context);
      }

      break;
    }

    case GST_MESSAGE_HAVE_CONTEXT:
    {
      GstContext *context;
      const gchar *context_type;
      gchar *context_str;

      gst_message_parse_have_context (msg, &context);

      context_type = gst_context_get_context_type (context);
      context_str =
          gst_structure_to_string (gst_context_get_structure (context));
      fprintf (stderr, "Got context from element '%s': %s=%s\n",
          GST_ELEMENT_NAME (GST_MESSAGE_SRC (msg)), context_type, context_str);
      g_free (context_str);
      gst_context_unref (context);
    }
      break;

    default:
      break;
  }

  return TRUE;
}
#endif

static gboolean        (*old_handle_message)  (GstRTSPMedia *media, GstMessage *message) = NULL;

static gboolean
_handle_message (GstRTSPMedia *media, GstMessage *msg)
{
  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      printf ("eos\n");
      break;

    case GST_MESSAGE_ERROR:
    {
      gchar *debug = NULL;
      GError *err = NULL;

      gst_message_parse_error (msg, &err, &debug);

      printf ("error '%s'\n", err->message);
      g_error_free (err);

      if (debug) {
        printf ("deails '%s'\n", debug);
        g_free (debug);
      }

      break;
    }

    case GST_MESSAGE_NEED_CONTEXT:
    {
      const gchar *context_type;

      gst_message_parse_context_type (msg, &context_type);
      fprintf (stderr, "got need context %s from element %s\n", context_type,
          GST_ELEMENT_NAME (GST_ELEMENT (msg->src)));

      if (g_strcmp0 (context_type, GST_GL_DISPLAY_CONTEXT_TYPE) == 0) {
        gst_element_set_context (GST_ELEMENT (msg->src), g_app.display_context);
      } else if (g_strcmp0 (context_type, "gst.gl.app_context") == 0) {
        gst_element_set_context (GST_ELEMENT (msg->src), g_app.app_context);
      }

      fprintf (stderr, "should have set the context\n");
      break;
    }

    /*
    case GST_MESSAGE_HAVE_CONTEXT:
    {
      GstContext *context;
      const gchar *context_type;
      gchar *context_str;

      gst_message_parse_have_context (msg, &context);

      context_type = gst_context_get_context_type (context);
      context_str =
          gst_structure_to_string (gst_context_get_structure (context));
      fprintf (stderr, "Got context from element '%s': %s=%s\n",
          GST_ELEMENT_NAME (GST_MESSAGE_SRC (msg)), context_type, context_str);
      g_free (context_str);
      gst_context_unref (context);
    }
      break;
      */

    default:
      break;
  }

  if (old_handle_message)
	  return old_handle_message(media, msg);

  return TRUE;
}

static void
_need_data_cb (GstAppSrc * appsrc, guint size, gpointer user_data)
{
  app_t *app = (app_t *) user_data;

  (void) appsrc;
  (void) size;

  if (app->sourceid == 0) {
    GST_DEBUG ("start feeding");
    app->sourceid = g_idle_add ((GSourceFunc) _gst_idle_loop, app);
  }
}

static void
_enough_data_cb (GstAppSrc * appsrc, gpointer user_data)
{
  app_t *app = (app_t *) user_data;

  (void) appsrc;

  if (app->sourceid != 0) {
    GST_DEBUG ("stop feeding");
    g_source_remove (app->sourceid);
    app->sourceid = 0;
  }
}

static void _media_configure(GstRTSPMediaFactory *factory, GstRTSPMedia *media, gpointer user_data)
{
	(void) factory;
  app_t *app = (app_t *)user_data;

    GstElement *element;

    /* override the bus handling for the media pieline */
  old_handle_message = GST_RTSP_MEDIA_GET_CLASS(media)->handle_message;
  GST_RTSP_MEDIA_GET_CLASS(media)->handle_message = _handle_message;

    // get the element used for providing the streams of the media
    element = gst_rtsp_media_get_element(media);

  app->pipeline = GST_PIPELINE(element);

  app->source = gst_bin_get_by_name (GST_BIN (app->pipeline), "source");
  g_assert (app->source);
  g_assert (GST_IS_APP_SRC (app->source));
  g_signal_connect (app->source, "need-data", G_CALLBACK (_need_data_cb), app);
  g_signal_connect (app->source, "enough-data", G_CALLBACK (_enough_data_cb),
      app);

#if 1
  /* Should we set the app context to the whoe pipeline? */
  gst_element_set_context (GST_ELEMENT (app->pipeline), g_app.app_context);
  /* XXX do we need to set the display context too? */
  gst_element_set_context (GST_ELEMENT (app->pipeline), g_app.display_context);

  /* NULL to PAUSED state pipeline to make sure the contexts are setup correctly? */
  gst_element_set_state (GST_ELEMENT (app->pipeline), GST_STATE_PAUSED);
#endif
}

#if 0
static void
_gst_start_pipeline (app_t * app)
{
  GstStateChangeReturn ret;

  /* Should we set the app context to the whoe pipeline? */
  gst_element_set_context (GST_ELEMENT (app->pipeline), g_app.app_context);
  /* XXX do we need to set the display context too? */
  gst_element_set_context (GST_ELEMENT (app->pipeline), g_app.display_context);

  /* NULL to PAUSED state pipeline to make sure the contexts are setup correctly? */
  gst_element_set_state (GST_ELEMENT (app->pipeline), GST_STATE_PAUSED);

  ret = gst_element_set_state (GST_ELEMENT (app->pipeline), GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_printerr ("Unable to set the pipeline to the playing state.\n");
    gst_object_unref (app->pipeline);
    fprintf (stderr, "failed to set pipeline to PLAYING\n");
    exit (EXIT_FAILURE);
  }
}
#endif

void _init_shaders_cb (GstGLContext * context, gpointer * userdata) {
  (void)context;
  (void)userdata;
  init();

  g_app.texture = scene_texture;
}

static void
_gst_init (int argc, char *argv[])
{
  g_app.width = width;
  g_app.height = height;

  gst_init (&argc, &argv);

 g_app.gl_display = gst_gl_display_new_with_type(GST_GL_DISPLAY_TYPE_X11);

 g_setenv ("GST_GL_PLATFORM", "glx", FALSE);
 g_setenv ("GST_GL_WINDOW", "x11", FALSE);
 //g_setenv ("GST_GL_API", "opengl", FALSE);

 g_app.gl_context = gst_gl_context_new(g_app.gl_display);
	
  g_app.display_context = gst_context_new (GST_GL_DISPLAY_CONTEXT_TYPE, TRUE);
  gst_context_set_gl_display (g_app.display_context, g_app.gl_display);

  GError *err = NULL;
  if (!gst_gl_context_create (g_app.gl_context, 0, &err)) {
    fprintf (stderr, "Failed to create GstGLContex context: %s\n",
        err->message);
    g_clear_error (&err);
    exit (EXIT_FAILURE);
  }

  gst_gl_display_add_context (g_app.gl_display, g_app.gl_context);

  g_app.app_context = gst_context_new ("gst.gl.app_context", TRUE);
  GstStructure *s = gst_context_writable_structure (g_app.app_context);
  gst_structure_set (s, "context", GST_TYPE_GL_CONTEXT, g_app.gl_context, NULL);

  /* 
   * Perform the OpenGL scene initialization in the OpenGL context associated
   * with the GstGL context.
   */
   gst_gl_context_thread_add (g_app.gl_context,
                               (GstGLContextThreadFunc) _init_shaders_cb, NULL);

  _gst_gl_init (&g_app, g_app.width, g_app.height);

  /* initialize gst_rtsp_server */

       GstRTSPServer       *server;
    GstRTSPMountPoints  *mounts;
    GstRTSPMediaFactory *factory;

   // create a server instance
    server = gst_rtsp_server_new();
    //gst_rtsp_server_set_address(server, "0.0.0.0");
    // gst_rtsp_server_set_service(server, std::to_string(self->mPort).c_str());

    mounts = gst_rtsp_server_get_mount_points(server);

  factory = gst_rtsp_media_factory_new();
    if (server == NULL || mounts == NULL || factory == NULL) {
        return;
    }

  char pl[1024];

  snprintf (pl, sizeof (pl),
      "( appsrc stream-type=0 do-timestamp=1 format=3 is-live=1 name=source "
      " caps=\"video/x-raw(memory:GLMemory), width=%d, height=%d, framerate=25/1, format=(string)RGBA, texture-target=(string)2D\" ! "
      " glvideoflip method=vertical-flip ! glcolorconvert ! gldownload ! video/x-raw,format=NV12 ! videorate ! "
      " x264enc tune=zerolatency speed-preset=veryfast ! h264parse ! "
      " rtph264pay name=pay0 pt=96 )",
      g_app.width, g_app.height);
  
  printf ("pipeline '%s'\n", pl);

    gst_rtsp_media_factory_set_launch(factory, pl);

    gst_rtsp_media_factory_set_shared (factory, TRUE);

        g_signal_connect(factory, "media-configure", (GCallback)_media_configure, &g_app);

    // attach the factory to the /gfx url
    gst_rtsp_mount_points_add_factory(mounts, "/opengl", factory);

    // don't need the ref to the mounts anymore
    g_object_unref(mounts);

  /* attach the server to the default maincontext */
  gst_rtsp_server_attach (server, NULL);


#if 1
    g_print("rtsp server type : %lu\n", gst_rtsp_server_get_type());
    g_print("rtsp server address : %s\n", gst_rtsp_server_get_address(server));
    g_print("rtsp server service : %s\n", gst_rtsp_server_get_service(server));
    g_print("rtsp server bound port : %d\n", gst_rtsp_server_get_bound_port(server));
#endif
}

static void
_gst_deinit (void)
{
  gst_gl_allocation_params_free ((GstGLAllocationParams *)
      g_app.allocation_params);
  gst_gl_context_destroy (g_app.gl_context);
}

int
main (int argc, char *argv[])
{
  signal(SIGINT, signal_handler);

  _gst_init (argc, argv);

  g_app.loop = g_main_loop_new (NULL, FALSE);
  g_unix_signal_add (SIGINT, signal_handler, g_app.loop);
  g_main_loop_run (g_app.loop);

  _gst_deinit ();

  deinit ();

  exit (EXIT_SUCCESS);
}
