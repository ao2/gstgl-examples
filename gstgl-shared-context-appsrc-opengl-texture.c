/*
 * gst-appsrc-opengl-texture - Render to a texture in OpenGL and pass it to GStreamer
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GLFW/glfw3.h>

#define GLFW_EXPOSE_NATIVE_X11 1
#define GLFW_EXPOSE_NATIVE_GLX 1
#include <GLFW/glfw3native.h>

#include <gst/gl/gl.h>
#include <gst/app/gstappsrc.h>
#include <gst/gl/x11/gstgldisplay_x11.h>
#include <gst/gl/gstglmemory.h>

#include "common-opengl-render-to-texture.h"

typedef struct app_t
{
  GLFWwindow *window;

  GstGLDisplay *gl_display;

  GstGLContext *gl_context;
  GstGLContext *my_context;
  GstContext *display_context;
  GstContext *app_context;

  GstGLVideoAllocationParams *allocation_params;

  int width;
  int height;

  GLuint framebuffer;
  GLuint texture;

  GstPipeline *pipeline;
  GMainLoop *loop;
  GstElement *source;
  guint sourceid;
} app_t;

app_t g_app = { 0 };

/* Prepare a GstBuffer, wrap an OpenGL texture into it. */
static GstBuffer *
wrap_gl_texture (app_t * app)
{
  GstGLMemoryAllocator *allocator;
  gpointer wrapped[1];
  GstGLFormat formats[1];
  GstBuffer *buffer;
  gboolean ret;

  allocator = gst_gl_memory_allocator_get_default (app->my_context);

  buffer = gst_buffer_new ();
  if (!buffer) {
    g_error ("Failed to create new buffer\n");
    return NULL;
  }

  wrapped[0] = (gpointer) app->texture;
  formats[0] = GST_GL_RGBA8;

  /* Wrap the texture into GLMemory. */
  ret = gst_gl_memory_setup_buffer (allocator, buffer, app->allocation_params,
      formats, wrapped, 1);
  if (!ret) {
    g_error ("Failed to setup gl memory\n");
    return NULL;
  }

  gst_object_unref (allocator);

  return buffer;
}

/* render the scene and push the buffer */
static void
_gst_gl_draw_frame (app_t * app)
{
  GstBuffer *buffer;
  GstFlowReturn ret;

  /* Render the scene. */
  glfwMakeContextCurrent (g_app.window);

  model_update ();
  draw_screen ();

  glfwSwapBuffers (g_app.window);
  glfwPollEvents ();

  glfwMakeContextCurrent (NULL);

  /* Wrap the rendered texture into a GstBuffer. */
  buffer = wrap_gl_texture (app);

  /* Sync on the buffer (this is probably most useful when using a buffer-pool). */
  GstGLSyncMeta *sync_meta = gst_buffer_get_gl_sync_meta (buffer);
  if (sync_meta) {
    gst_gl_sync_meta_set_sync_point (sync_meta, app->my_context);
    gst_gl_sync_meta_wait (sync_meta, app->my_context);
  }

  /* Push the buffer. */
  ret = gst_app_src_push_buffer (GST_APP_SRC (app->source), buffer);
  if (ret != GST_FLOW_OK) {
    g_printerr ("GST_FLOW != OK, return value is %d\n", ret);
  }
}

static gboolean
_gst_idle_loop (gpointer data)
{
  app_t *app = (app_t *) data;
  if (glfwWindowShouldClose (app->window)) {
    g_main_loop_quit (app->loop);
    return FALSE;
  }

  _gst_gl_draw_frame (app);
  return TRUE;
}

static void
_gst_gl_init (app_t * app, int width, int height)
{
  GstVideoInfo *vinfo = gst_video_info_new ();

  gboolean ret =
      gst_video_info_set_format (vinfo, GST_VIDEO_FORMAT_RGBA, width, height);
  if (!ret) {
    fprintf (stderr, "set video info ret %d\n", ret);
    exit (EXIT_FAILURE);
  }

  app->allocation_params =
      gst_gl_video_allocation_params_new_wrapped_texture (app->my_context, NULL,
      vinfo, 0, NULL, GST_GL_TEXTURE_TARGET_2D, GST_GL_RGBA8, app->texture,
      NULL, 0);
}

static gboolean
_gst_bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
  app_t *app = (app_t *) data;

  (void) bus;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      fprintf (stderr, "eos\n");
      g_main_loop_quit (app->loop);
      break;

    case GST_MESSAGE_ERROR:
    {
      gchar *debug = NULL;
      GError *err = NULL;

      gst_message_parse_error (msg, &err, &debug);

      fprintf (stderr, "error '%s'\n", err->message);
      g_error_free (err);

      if (debug) {
        fprintf (stderr, "deails '%s'\n", debug);
        g_free (debug);
      }

      if (app->loop)
        g_main_loop_quit (app->loop);
      else
        exit (EXIT_FAILURE);
      break;
    }

    case GST_MESSAGE_NEED_CONTEXT:
    {
      const gchar *context_type;

      gst_message_parse_context_type (msg, &context_type);
      fprintf (stderr, "got need context %s from element %s\n", context_type,
          GST_ELEMENT_NAME (GST_ELEMENT (msg->src)));

      if (g_strcmp0 (context_type, GST_GL_DISPLAY_CONTEXT_TYPE) == 0) {
        gst_element_set_context (GST_ELEMENT (msg->src), g_app.display_context);
      } else if (g_strcmp0 (context_type, "gst.gl.app_context") == 0) {
        gst_element_set_context (GST_ELEMENT (msg->src), g_app.app_context);
      }

      break;
    }

    case GST_MESSAGE_HAVE_CONTEXT:
    {
      GstContext *context;
      const gchar *context_type;
      gchar *context_str;

      gst_message_parse_have_context (msg, &context);

      context_type = gst_context_get_context_type (context);
      context_str =
          gst_structure_to_string (gst_context_get_structure (context));
      fprintf (stderr, "Got context from element '%s': %s=%s\n",
          GST_ELEMENT_NAME (GST_MESSAGE_SRC (msg)), context_type, context_str);
      g_free (context_str);
      gst_context_unref (context);
    }
      break;

    default:
      break;
  }

  return TRUE;
}

static void
_need_data_cb (GstAppSrc * appsrc, guint size, gpointer user_data)
{
  app_t *app = (app_t *) user_data;

  (void) appsrc;
  (void) size;

  if (app->sourceid == 0) {
    GST_DEBUG ("start feeding");
    app->sourceid = g_idle_add ((GSourceFunc) _gst_idle_loop, app);
  }
}

static void
_enough_data_cb (GstAppSrc * appsrc, gpointer user_data)
{
  app_t *app = (app_t *) user_data;

  (void) appsrc;

  if (app->sourceid != 0) {
    GST_DEBUG ("stop feeding");
    g_source_remove (app->sourceid);
    app->sourceid = 0;
  }
}

static void
_gst_load_pipeline (app_t * app, const char *text)
{
  app->pipeline = GST_PIPELINE (gst_parse_launch (text, NULL));

  app->source = gst_bin_get_by_name (GST_BIN (app->pipeline), "source");
  g_assert (app->source);
  g_assert (GST_IS_APP_SRC (app->source));
  g_signal_connect (app->source, "need-data", G_CALLBACK (_need_data_cb), app);
  g_signal_connect (app->source, "enough-data", G_CALLBACK (_enough_data_cb),
      app);

  GstBus *bus = gst_pipeline_get_bus (GST_PIPELINE (app->pipeline));
  gst_bus_add_watch (bus, _gst_bus_call, app);
  gst_object_unref (bus);
}

static void
_gst_start_pipeline (app_t * app)
{
  GstStateChangeReturn ret;

  /* Should we set the app context to the whoe pipeline? */
  gst_element_set_context (GST_ELEMENT (app->pipeline), g_app.app_context);
  /* XXX do we need to set the display context too? */
  gst_element_set_context (GST_ELEMENT (app->pipeline), g_app.display_context);

  /* NULL to PAUSED state pipeline to make sure the contexts are setup correctly? */
  gst_element_set_state (GST_ELEMENT (app->pipeline), GST_STATE_PAUSED);

  ret = gst_element_set_state (GST_ELEMENT (app->pipeline), GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_printerr ("Unable to set the pipeline to the playing state.\n");
    gst_object_unref (app->pipeline);
    fprintf (stderr, "failed to set pipeline to PLAYING\n");
    exit (EXIT_FAILURE);
  }
}


static void
_gst_init (int argc, char *argv[])
{
  g_app.width = width;
  g_app.height = height;

  gst_init (&argc, &argv);

  Display *dpy = glfwGetX11Display ();

  g_app.gl_display = (GstGLDisplay *) gst_gl_display_x11_new_with_display (dpy);

  GstGLPlatform gl_platform = GST_GL_PLATFORM_GLX;
  guintptr gl_handle = gst_gl_context_get_current_gl_context (gl_platform);
  GstGLAPI gl_api = gst_gl_context_get_current_gl_api (gl_platform, NULL, NULL);
  g_app.gl_context =
      gst_gl_context_new_wrapped (g_app.gl_display,
      gl_handle, gl_platform, gl_api);

  /* Create a new non-wrapped local GstGlContext to share with other elements. */
  GError *err = NULL;
  if (!gst_gl_display_create_context (g_app.gl_display,
          g_app.gl_context, &g_app.my_context, &err)) {
    fprintf (stderr, "Failed to create GstGLContex context: %s\n",
        err->message);
    g_clear_error (&err);
    exit (EXIT_FAILURE);
  }

  gst_gl_display_add_context (g_app.gl_display, g_app.my_context);

  g_app.display_context = gst_context_new (GST_GL_DISPLAY_CONTEXT_TYPE, TRUE);
  gst_context_set_gl_display (g_app.display_context, g_app.gl_display);

  g_app.app_context = gst_context_new ("gst.gl.app_context", TRUE);
  GstStructure *s = gst_context_writable_structure (g_app.app_context);
  gst_structure_set (s, "context", GST_TYPE_GL_CONTEXT, g_app.my_context, NULL);

  /* Since the GstGlContext was created from a wrapped context, OpenGL
   * resources like textures are accessible from GstGl directly.
   *
   * For example there is no need to rely on gst_gl_context_thread_add() to
   * render explicitly in the GstGlContext thread.
   */
  gst_gl_context_activate (g_app.my_context, TRUE);

  char pl[1024];

  snprintf (pl, sizeof (pl),
      "appsrc stream-type=0 do-timestamp=1 format=3 is-live=1 name=source "
      " caps=\"video/x-raw(memory:GLMemory), width=%d, height=%d, format=(string)RGBA, texture-target=(string)2D\" ! "
      " glvideoflip method=vertical-flip ! glcolorconvert ! gldownload ! "
      " x264enc tune=zerolatency speed-preset=veryfast ! h264parse ! queue !"
      " matroskamux ! filesink location=\"/tmp/test.mkv\"",
      g_app.width, g_app.height);
  printf ("pipeline '%s'\n", pl);

  _gst_load_pipeline (&g_app, pl);

  _gst_gl_init (&g_app, g_app.width, g_app.height);

  _gst_start_pipeline (&g_app);
}

static void
_gst_deinit (void)
{
  gst_app_src_end_of_stream (GST_APP_SRC (g_app.source));
  gst_element_set_state (GST_ELEMENT (g_app.pipeline), GST_STATE_NULL);
  gst_gl_allocation_params_free ((GstGLAllocationParams *)
      g_app.allocation_params);
  gst_gl_context_destroy (g_app.my_context);
}

int
main (int argc, char *argv[])
{
  (void) argc;
  GLFWwindow *window;

  glfwSetErrorCallback (error_callback);

  if (!glfwInit ())
    exit (EXIT_FAILURE);

  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint (GLFW_RESIZABLE, 0);

  window = glfwCreateWindow (width, height, argv[0], NULL, NULL);
  if (!window) {
    glfwTerminate ();
    exit (EXIT_FAILURE);
  }

  glfwSetKeyCallback (window, input_cb);

  glfwMakeContextCurrent (window);
  glfwSwapInterval (1);

  glEnable (GL_DEBUG_OUTPUT);
  glDebugMessageCallback (debug_callback, NULL);

  init ();

  g_app.window = window;
  g_app.texture = scene_texture;
  _gst_init (argc, argv);

  g_app.loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (g_app.loop);

  _gst_deinit ();

  deinit ();

  glfwDestroyWindow (window);

  glfwTerminate ();
  exit (EXIT_SUCCESS);
}
