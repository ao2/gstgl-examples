/*
 * GStreamer
 * Copyright (C) 2008-2009 Julien Isorce <julien.isorce@gmail.com>
 * Copyright 2021, Collabora Ltd
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <gst/gl/gstglfuncs.h>
#include <gst/gst.h>

#include <stdio.h>
#include <stdbool.h>

#include "gears.h"

static int width = 640;
static int height = 480;

static gboolean
bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
	(void)bus;
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      g_print ("End-of-stream\n");
      g_main_loop_quit (loop);
      break;
    case GST_MESSAGE_ERROR:
    {
      gchar *debug = NULL;
      GError *err = NULL;

      gst_message_parse_error (msg, &err, &debug);

      g_print ("Error: %s\n", err->message);
      g_error_free (err);

      if (debug) {
        g_print ("Debug details: %s\n", debug);
        g_free (debug);
      }

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

static void
draw_texture (GLuint texture)
{
  glDisable (GL_DEPTH_TEST);

  glMatrixMode (GL_PROJECTION);
  glPushMatrix ();
  glLoadIdentity ();

  glMatrixMode (GL_MODELVIEW);
  glPushMatrix ();
  glLoadIdentity ();

  glEnable (GL_TEXTURE_2D);
  glBindTexture (GL_TEXTURE_2D, texture);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  glBegin (GL_QUADS);
    glTexCoord2f (0.0f, 0.0f); glVertex3f (-1.0f, -1.0f, 0.0f);
    glTexCoord2f (1.0f, 0.0f); glVertex3f ( 1.0f, -1.0f, 0.0f);
    glTexCoord2f (1.0f, 1.0f); glVertex3f ( 1.0f,  1.0f, 0.0f);
    glTexCoord2f (0.0f, 1.0f); glVertex3f (-1.0f,  1.0f, 0.0f);
  glEnd ();

  glBindTexture (GL_TEXTURE_2D, 0);
  glDisable (GL_TEXTURE_2D);

  glPopMatrix ();

  glMatrixMode (GL_PROJECTION);
  glPopMatrix ();

  glEnable (GL_DEPTH_TEST);
}

//client draw callback
static gboolean
drawCallback (void *filter, GLuint texture, GLuint width, GLuint height,
    gpointer data)
{
  (void)filter;
  (void)data;

  static GstClockTime current_time;
  static GstClockTime last_time = GST_CLOCK_TIME_NONE;
  static gint nbFrames = 0;
  static bool inited = false;

  static double tRot0 = -1.0;
  double dt, t;

  current_time = gst_util_get_timestamp ();
  nbFrames++;

  if ((current_time - last_time) >= GST_SECOND) {
    printf("GRAPHIC FPS = %d\n", nbFrames);
    nbFrames = 0;
    last_time = current_time;
  }

  t = (double) current_time / GST_SECOND;

  if (tRot0 < 0.0)
    tRot0 = t;
  dt = t - tRot0;
  tRot0 = t;

  /* advance rotation for next frame */
  angle += 70.0 * dt;           /* 70 degrees per second */
  if (angle > 3600.0)
    angle -= 3600.0;

  glEnable (GL_DEPTH_TEST);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /*
   * TODO do initialization only once in some initial setup,
   * maybe when creation of a new GstGLContext is reported in the bus callback.
   */
  if (!inited) {
    reshape (width, height);

    /*
     * Apparently GstGL will invert the Y axis,
     * So flip it before setting up the lights in init().
     */
    glScalef (1.0, -1.0, 1.0);

    init ();
    inited = true;
  }
  /* Draw the input texture */
  draw_texture (texture);

  /* Set perspective projection again */
  reshape (width, height);

  /*
   * Apparently GstGL will invert the Y axis,
   * so render the scene upside down to compensate.
   */
  glScalef (1.0, -1.0, 1.0);

  glTranslatef (0.0, 0.0, -5.0);
  draw_gears ();

  glDisable (GL_DEPTH_TEST);

  //return TRUE because we dealt with the texture
  return TRUE;
}

gint
main (gint argc, gchar * argv[])
{
  GstStateChangeReturn ret;
  GstElement *pipeline, *glfilterapp;
  GMainLoop *loop;
  GstBus *bus;

  /* FIXME: remove once the example supports gl3 and/or gles2 */
  g_setenv ("GST_GL_API", "opengl", FALSE);

  /* initialization */
  gst_init (&argc, &argv);
  loop = g_main_loop_new (NULL, FALSE);

  gchar *pipeline_string =
      g_strdup_printf
      ("videotestsrc num-buffers=400 ! video/x-raw,width=%d,height=%d,framerate=25/1 !"
       " glupload ! glfilterapp name=filterapp ! glcolorconvert ! gldownload !"
       " avenc_mpeg4 ! avimux ! filesink location=gstgears.avi",
       width, height);

  /* create pipeline */
  pipeline = gst_parse_launch (pipeline_string, NULL);
  g_free (pipeline_string);

  glfilterapp = gst_bin_get_by_name (GST_BIN (pipeline), "filterapp");
  g_assert (glfilterapp);

  g_signal_connect (G_OBJECT (glfilterapp), "client-draw",
      G_CALLBACK (drawCallback), NULL);

  /* watch for messages on the pipeline's bus (note that this will only
   * work like this when a GLib main loop is running) */
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  gst_bus_add_watch (bus, bus_call, loop);
  gst_object_unref (bus);

  /* run */
  ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_print ("Failed to start up pipeline!\n");

    /* check if there is an error message with details on the bus */
    GstMessage *msg = gst_bus_poll (bus, GST_MESSAGE_ERROR, 0);
    if (msg) {
      GError *err = NULL;

      gst_message_parse_error (msg, &err, NULL);
      g_warning ("ERROR: %s", err->message);
      g_error_free (err);
      gst_message_unref (msg);
    }
    return -1;
  }

  g_main_loop_run (loop);

  printf("result written in gstgears.avi\n");

  /* clean up */
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  return 0;
}
